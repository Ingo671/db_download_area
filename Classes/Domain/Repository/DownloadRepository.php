<?php
namespace DerBergmann\DbDownloadArea\Domain\Repository;


/***
 *
 * This file is part of the "DownloadArea" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Philippe Wegener <p.wegener@der-bergmann.net>, Der-Bergmann
 *
 ***/

/**
 *
 *
 * @package tk_schwab_downloads
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class DownloadRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * takes a comma seperated list of uids and returns the selected records in the corresponding order
	 * ...it sucks having to do it this way :(
	 * 
	 * @param string $uidList
	 * @return array
	 */
	public function findForUidList($uidList) 
	{
		$uidArray = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $uidList);
		
		$result = array();
		foreach ($uidArray as $uid) {
			$download = $this->findByUid($uid);
			if ($download !== NULL) {
				$result[] = $download;
			}
		}
		
		return $result;
	}
	
}
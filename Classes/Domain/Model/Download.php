<?php
namespace DerBergmann\DbDownloadArea\Domain\Model;


/***
 *
 * This file is part of the "DownloadArea" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Philippe Wegener <p.wegener@der-bergmann.net>, Der-Bergmann
 *
 ***/
/**
 * Download
 */
class Download extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

	const ACCESS_LEVEL_PUBLIC = 0;
	const ACCESS_LEVEL_ALL_CUSTOMERS = 1;
/**	const ACCESS_LEVEL_SELECTED_CUSTOMERS = 2; **/

    /**
     * title
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * datei
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $datei = null;

    /**
     * zugriff
     * 
     * @var int
     */
    protected $zugriff = 0;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the datei
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $datei
     */
    public function getDatei()
    {
        return $this->datei;
    }

    /**
     * Sets the datei
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $datei
     * @return void
     */
    public function setDatei(\TYPO3\CMS\Extbase\Domain\Model\FileReference $datei)
    {
        $this->datei = $datei;
    }

    /**
     * Returns the zugriff
     * 
     * @return int $zugriff
     */
    public function getZugriff()
    {
        return $this->zugriff;
    }

    /**
     * Sets the zugriff
     * 
     * @param int $zugriff
     * @return void
     */
    public function setZugriff($zugriff)
    {
        $this->zugriff = $zugriff;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * returns true if the download has public access level
     *
     *@return boolean
     */
    public function getIsPublic()
    {
    	return $this->getZugriff() === self::ACCESS_LEVEL_PUBLIC;
    } 
    
}

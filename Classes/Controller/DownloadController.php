<?php

namespace DerBergmann\DbDownloadArea\Controller;


/***
 *
 * This file is part of the "DownloadArea" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Philippe Wegener <p.wegener@der-bergmann.net>, Der-Bergmann
 *
 ***/
/**
 * DownloadController
 */

use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Security\Cryptography\HashService;
use TYPO3\CMS\Extbase\Security\Exception\InvalidArgumentForHashGenerationException;
use TYPO3\CMS\Extbase\Security\Exception\InvalidHashException;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
	 
class DownloadController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    /**
     * downloadRepository
     * 
     * @var \DerBergmann\DbDownloadArea\Domain\Repository\DownloadRepository
     * @inject
     */
    protected $downloadRepository;
    
// 	/**
// 	 * hashService
// 	 *
// 	 * @var TYPO3\CMS\Extbase\Security\Cryptography\HashService
// 	 * @inject 
// 	 */
// 	protected $hashService;	

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
		if (isset($this->settings['download_list']['downloadarea']) && strlen($this->settings['download_list']['downloadarea']) > 0) {
			$download = $this->downloadRepository->findForUidList($this->settings['download_list']['downloadarea']);
		} else {
			$download = $this->downloadRepository->findAll();
		}
		
		$this->view->assign('downloadarea', $download);
	}


   
	/**
	 * checks if the download may be accessed
	 * 
	 * @param \DerBergmann\DbDownloadArea\Domain\Model\Download $download
	 * @param string $customerCode
	 * @return boolean
	 */    
    public function hasAccess(\DerBergmann\DbDownloadArea\Domain\Model\Download $download, $customerCode = '')
    {
    	/*$this->view->assign('download', $download);
    	$this->view->assign('legitimation', $legitimation);*/

		if ($download->getZugriff() === \DerBergmann\DbDownloadArea\Domain\Model\Download::ACCESS_LEVEL_PUBLIC)
		{
			return TRUE;
		}
		
		if ($download->getZugriff() === \DerBergmann\DbDownloadArea\Domain\Model\Download::ACCESS_LEVEL_ALL_CUSTOMERS) {
			$customercode = $this->customerCodeRepository->findOneByValue($customerCode);
			if ($customercode !== NULL) {
				return TRUE;
			}
		}
		
		if ($download->getZugriff() === \DerBergmann\DbDownloadArea\Domain\Model\Download::ACCESS_LEVEL_SELECTED_CUSTOMERS) {
			$customercode = $this->customerCodeRepository->findOneByValue($customerCode);
			if ($download->getCustomerCodes()->contains($customercode)) {
				return TRUE;
			}
		}
		
		return FALSE;
	}
}
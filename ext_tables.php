<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

//        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//            'DerBergmann.DbDownloadArea',
//            'Downloadarea',
//            'Download Bereich'
//        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('db_download_area', 'Configuration/TypoScript', 'DownloadArea');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_dbdownloadarea_domain_model_download', 'EXT:db_download_area/Resources/Private/Language/locallang_csh_tx_dbdownloadarea_domain_model_download.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dbdownloadarea_domain_model_download');

    }
);

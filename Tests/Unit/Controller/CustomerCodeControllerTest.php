<?php
namespace DerBergmann\DbDownloadArea\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Philippe Wegener <p.wegener@der-bergmann.net>
 */
class CustomerCodeControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \DerBergmann\DbDownloadArea\Controller\CustomerCodeController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\DerBergmann\DbDownloadArea\Controller\CustomerCodeController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCustomerCodesFromRepositoryAndAssignsThemToView()
    {

        $allCustomerCodes = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $customerCodeRepository = $this->getMockBuilder(\::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $customerCodeRepository->expects(self::once())->method('findAll')->will(self::returnValue($allCustomerCodes));
        $this->inject($this->subject, 'customerCodeRepository', $customerCodeRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('customerCodes', $allCustomerCodes);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenCustomerCodeToView()
    {
        $customerCode = new \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('customerCode', $customerCode);

        $this->subject->showAction($customerCode);
    }
}

<?php
namespace DerBergmann\DbDownloadArea\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Philippe Wegener <p.wegener@der-bergmann.net>
 */
class DownloadControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \DerBergmann\DbDownloadArea\Controller\DownloadController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\DerBergmann\DbDownloadArea\Controller\DownloadController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllDownloadsFromRepositoryAndAssignsThemToView()
    {

        $allDownloads = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $downloadRepository = $this->getMockBuilder(\DerBergmann\DbDownloadArea\Domain\Repository\DownloadRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $downloadRepository->expects(self::once())->method('findAll')->will(self::returnValue($allDownloads));
        $this->inject($this->subject, 'downloadRepository', $downloadRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('downloads', $allDownloads);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenDownloadToView()
    {
        $download = new \DerBergmann\DbDownloadArea\Domain\Model\Download();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('download', $download);

        $this->subject->showAction($download);
    }
}

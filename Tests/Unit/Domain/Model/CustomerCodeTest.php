<?php
namespace DerBergmann\DbDownloadArea\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Philippe Wegener <p.wegener@der-bergmann.net>
 */
class CustomerCodeTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getValueReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getValue()
        );
    }

    /**
     * @test
     */
    public function setValueForStringSetsValue()
    {
        $this->subject->setValue('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'value',
            $this->subject
        );
    }
}

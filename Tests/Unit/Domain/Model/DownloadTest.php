<?php
namespace DerBergmann\DbDownloadArea\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Philippe Wegener <p.wegener@der-bergmann.net>
 */
class DownloadTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \DerBergmann\DbDownloadArea\Domain\Model\Download
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \DerBergmann\DbDownloadArea\Domain\Model\Download();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateiReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getDatei()
        );
    }

    /**
     * @test
     */
    public function setDateiForFileReferenceSetsDatei()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setDatei($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'datei',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZugriffReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getZugriff()
        );
    }

    /**
     * @test
     */
    public function setZugriffForIntSetsZugriff()
    {
        $this->subject->setZugriff(12);

        self::assertAttributeEquals(
            12,
            'zugriff',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCustomercodeReturnsInitialValueForCustomerCode()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCustomercode()
        );
    }

    /**
     * @test
     */
    public function setCustomercodeForObjectStorageContainingCustomerCodeSetsCustomercode()
    {
        $customercode = new \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode();
        $objectStorageHoldingExactlyOneCustomercode = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCustomercode->attach($customercode);
        $this->subject->setCustomercode($objectStorageHoldingExactlyOneCustomercode);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneCustomercode,
            'customercode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addCustomercodeToObjectStorageHoldingCustomercode()
    {
        $customercode = new \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode();
        $customercodeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $customercodeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($customercode));
        $this->inject($this->subject, 'customercode', $customercodeObjectStorageMock);

        $this->subject->addCustomercode($customercode);
    }

    /**
     * @test
     */
    public function removeCustomercodeFromObjectStorageHoldingCustomercode()
    {
        $customercode = new \DerBergmann\DbDownloadArea\Domain\Model\CustomerCode();
        $customercodeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $customercodeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($customercode));
        $this->inject($this->subject, 'customercode', $customercodeObjectStorageMock);

        $this->subject->removeCustomercode($customercode);
    }
}

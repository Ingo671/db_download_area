<?php
defined('TYPO3_MODE') or die();

$extKey = 'db_download_area';

// register plugin
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'DerBergmann.DbDownloadArea',
            'Downloadarea',
            'Download Bereich'
        );

//flexform
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
  'db_download_area',
  'Configuration/TypoScript',
  'DownloadArea'
  );
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

$pluginSignature = str_replace('_', '', $extKey) . '_downloadarea';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dbdownloadarea_downloadarea'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['dbdownloadarea_downloadarea'] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'dbdownloadarea_downloadarea',
    'FILE:EXT:' . $extKey . '/Configuration/FlexForms/Flexform.xml'
);

?>
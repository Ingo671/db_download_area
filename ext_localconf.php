<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'DerBergmann.DbDownloadArea',
            'Downloadarea',
            [
                'Download' => 'list, access, download, generate'
            ],
            // non-cacheable actions
            [
                'Download' => 'access, download, generate',
                'CustomerCode' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    downloadarea {
                        iconIdentifier = db_download_area-plugin-downloadarea
                        title = LLL:EXT:db_download_area/Resources/Private/Language/locallang_db.xlf:tx_db_download_area_downloadarea.name
                        description = LLL:EXT:db_download_area/Resources/Private/Language/locallang_db.xlf:tx_db_download_area_downloadarea.description
                        tt_content_defValues {
                            CType = list
                            list_type = dbdownloadarea_downloadarea
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'db_download_area-plugin-downloadarea',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:db_download_area/Resources/Public/Icons/user_plugin_downloadarea.svg']
			);
		
    }
);
